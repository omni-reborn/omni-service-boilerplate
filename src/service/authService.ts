import axios from 'axios';
import { injectable, inject } from 'inversify';
import { CONFIG_IDENTIFIER, FACTORY_IDENTIFIER } from 'src/constants';
import { IAuthServiceConfig } from 'src/config/service';
import { ConnectionError, UnspecifiedError } from 'src/error/core';
import { exists } from 'src/util';
import { AuthorizationError } from 'src/error/core/authenticationError';
import { ILoggerFactory, ILogger } from 'src/logger';

export interface UserRole {
  name: string;
  permissions: Array<string>;
}

export interface UserDetails {
  username: string;
  isAuthenticated: boolean;
  roles: Array<UserRole>;
}


export interface IAuthService {
  getUserDetails(token: string, correlationId: string): Promise<UserDetails>;
}

@injectable()
export class AuthService implements IAuthService {
  private readonly config: IAuthServiceConfig;
  private readonly logger: ILogger;

  public constructor(
    @inject(CONFIG_IDENTIFIER.IAuthServiceConfig) config: IAuthServiceConfig,
    @inject(FACTORY_IDENTIFIER.ILoggerFactory) loggerFactory: ILoggerFactory,
  ) {
    this.config = config;
    this.logger = loggerFactory.createLogger('authService');
  }

  public async getUserDetails(token: string, correlationId: string) {
    try {
      const authRoute = `${this.config['auth-service-dns']}/user/details`;
      const response = await axios.post<UserDetails>(authRoute, { userToken: token });
      return response.data;
    } catch (error) {
      if (exists(error.response)) {
        const message = 'Service was not authorized to POST auth response. Make sure that service permissions are correct.';
        this.logger.error(message, { correlationId });
        throw new AuthorizationError(message);
      } else if (error.request) {
        const message = 'Did not get response from auth service. Check service connection.';
        this.logger.error(message, { correlationId });
        throw new ConnectionError(message);
      } else {
        this.logger.error('An unexpected error occured.', { correlationId });
        throw new UnspecifiedError();
      }
    }
  }
}
