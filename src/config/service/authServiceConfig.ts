import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TAuthServiceConfig>;
export interface IAuthServiceConfig extends T { }

export const TAuthServiceConfig = t.type({
  'auth-service-dns': t.string,
});
