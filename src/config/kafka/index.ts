import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TKafkaConnectorConfig>;
export interface IKafkaConnectorConfig extends T { }

export const TKafkaConnectorConfig = t.type({
  'client-id': t.string,
  brokers: t.array(t.string),
});
