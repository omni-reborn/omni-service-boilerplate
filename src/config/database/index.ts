import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TDatabaseConnectorConfig>;
export interface IDatabaseConnectorConfig extends T { }

export const TDatabaseConnectorConfig = t.type({
  type: t.keyof({
    mysql: undefined,
    mariadb: undefined,
    postgres: undefined,
    sqlite: undefined,
  }),
  host: t.string,
  port: t.string,
  username: t.string,
  password: t.string,
  database: t.string,
});
