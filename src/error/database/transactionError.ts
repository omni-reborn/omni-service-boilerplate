import { DatabaseError } from './databaseError';

export class TransactionError extends DatabaseError {
  public constructor(message = 'Failed to create a transaction.', error?: any) {
    super(message, error);
  }
}
