import { BaseError } from 'src/error/core';

export class DatabaseError extends BaseError {
  public constructor(message = 'Internal Database Error.', error?: any) {
    super(message, error);
  }
}
