import { DatabaseError } from './databaseError';

export class DatabaseRepeatConnectionError extends DatabaseError {
  public constructor(message = 'Establishing more than one connection to the database is not allowed.', error?: any) {
    super(message, error);
  }
}
