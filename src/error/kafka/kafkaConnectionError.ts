import { KafkaError } from './kafkaError';

export class KafkaConnectionError extends KafkaError {
  public constructor(message = 'Cannot connect to kafka cluster.', error?: any) {
    super(message, error);
  }
}
