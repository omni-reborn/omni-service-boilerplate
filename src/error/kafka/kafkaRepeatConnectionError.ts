import { KafkaError } from './kafkaError';

export class KafkaRepeatConnectionError extends KafkaError {
  public constructor(message = 'Establishing more than one connection to the Kafka cluster is not allowed.', error?: any) {
    super(message, error);
  }
}
