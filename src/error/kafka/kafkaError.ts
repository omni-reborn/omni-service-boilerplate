import { BaseError } from 'src/error/core';

export class KafkaError extends BaseError {
  public constructor(message = 'Internal Kafka Error.', error?: any) {
    super(message, error);
  }
}
