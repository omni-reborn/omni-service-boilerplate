import { BaseError } from './baseError';

export class NotFoundError extends BaseError {
  public constructor(message = 'Resource not found', error?: any) {
    super(message, error);
  }
}
