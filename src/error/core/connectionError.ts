import { BaseError } from './baseError';

export class ConnectionError extends BaseError {
  public constructor(message = 'Cannot connect to a remote server.', error?: any) {
    super(message, error);
  }
}
