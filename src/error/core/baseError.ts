export class BaseError extends Error {
  public readonly message: string;
  public readonly error: any;

  public constructor(message: string, error: any) {
    super(message);

    this.error = error;
    this.name = this.constructor.name;
  }
}
