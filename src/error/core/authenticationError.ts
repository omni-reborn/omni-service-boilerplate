import { BaseError } from './baseError';

export class AuthorizationError extends BaseError {
  public constructor(message = 'Service was not authorized to make that request.', error?: any) {
    super(message, error);
  }
}
