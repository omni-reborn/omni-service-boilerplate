import { BaseError } from './baseError';

export class ConfigurationError extends BaseError {
  public constructor(message = 'Invalid configuration file.', error?: any) {
    super(message, error);
  }
}
