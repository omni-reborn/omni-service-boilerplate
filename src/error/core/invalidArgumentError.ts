import { BaseError } from './baseError';

export class InvalidArgumentError extends BaseError {
  public constructor(message = 'Invalid argument.', error?: any) {
    super(message, error);
  }
}
