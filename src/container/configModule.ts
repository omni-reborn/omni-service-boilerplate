import { ContainerModule, interfaces } from 'inversify';
import { IDatabaseConnectorConfig, TDatabaseConnectorConfig } from 'src/config/database';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { loadConfig } from 'src/config/core';
import { IKafkaConnectorConfig, TKafkaConnectorConfig } from 'src/config/kafka';
import { IAppServiceConfig, TAppServiceConfig } from 'src/config/app';
import { ILoggerConfig, TLoggerConfig } from 'src/config/logger';
import { IAuthServiceConfig, TAuthServiceConfig } from 'src/config/service';
import { IUserAuthProviderConfig, TUserAuthProviderConfig } from 'src/config/middleware/userAuthProviderConfig';

export const configModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IDatabaseConnectorConfig>(CONFIG_IDENTIFIER.IDatabaseConnectorConfig)
      .toConstantValue(loadConfig<IDatabaseConnectorConfig>(TDatabaseConnectorConfig, './config/databaseConnectorConfig.yml'));

    bind<IKafkaConnectorConfig>(CONFIG_IDENTIFIER.IKafkaConnectorConfig)
      .toConstantValue(loadConfig<IKafkaConnectorConfig>(TKafkaConnectorConfig, './config/kafkaConnectorConfig.yml'));

    bind<IAppServiceConfig>(CONFIG_IDENTIFIER.IAppServiceConfig)
      .toConstantValue(loadConfig<IAppServiceConfig>(TAppServiceConfig, './config/appServiceConfig.yml'));

    bind<IAuthServiceConfig>(CONFIG_IDENTIFIER.IAuthServiceConfig)
      .toConstantValue(loadConfig<IAuthServiceConfig>(TAuthServiceConfig, './config/authServiceConfig.yml'));

    bind<ILoggerConfig>(CONFIG_IDENTIFIER.IAppServiceConfig)
      .toConstantValue(loadConfig<ILoggerConfig>(TLoggerConfig, './config/loggerConfig.yml'));

    bind<IUserAuthProviderConfig>(CONFIG_IDENTIFIER.IUserAuthProviderConfig)
      .toConstantValue(loadConfig<IUserAuthProviderConfig>(TUserAuthProviderConfig, './config/userAuthProviderConfig.yml'));
  },
);
