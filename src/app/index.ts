import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import { Application } from 'express';
import { Server as HttpServer } from 'http';
import { InversifyExpressServer } from 'inversify-express-utils';
import { Namespace, createNamespace } from 'cls-hooked';
import { IAppServiceConfig } from 'src/config/app';
import { APP_IDENTIFIER } from 'src/constants';
import { injectable } from 'inversify';
import { ILogger } from 'src/logger';
import { LoggerMiddleWare } from 'src/middleware/logger';

export interface IAppService {
  start(): Promise<void>;
  stop(): Promise<void>;
}

@injectable()
export class AppService implements IAppService {
  private readonly config: IAppServiceConfig;
  private readonly logger: ILogger;
  private readonly context: Namespace;
  private readonly express: Application;

  private server: HttpServer;

  public constructor(rawServer: InversifyExpressServer, logger: ILogger, config: IAppServiceConfig) {
    this.config = config;
    this.logger = logger;
    this.context = createNamespace(APP_IDENTIFIER.clsContext);

    const requestMiddlewareLogger = this.logger.createChildLogger('request');
    const requestLoggingMiddleWare = new LoggerMiddleWare(requestMiddlewareLogger);

    rawServer.setConfig((express) => {
      express.use(requestLoggingMiddleWare.handle);
      express.use(cookieParser());
      express.use(bodyParser.json());
      express.use(bodyParser.urlencoded({ extended: true }));

      // Run every request in CLS transactional context scope
      express.use((req, res, next) => {
        this.context.run(() => next());
      });
    });

    this.express = rawServer.build();
  }

  public async start() {
    this.server = this.express.listen(parseInt(this.config.port, 10));
    this.logger.info('Service has been started.');

    process.on('exit', () => this.stop());
  }

  public async stop() {
    process.removeAllListeners();

    this.logger.info('Service has been stopped.');
    this.server.close();
  }
}
