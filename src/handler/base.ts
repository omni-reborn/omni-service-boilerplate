import { Consumer, ConsumerConfig } from 'kafkajs';
import { unmanaged, injectable } from 'inversify';
import { IConsumerFactory } from 'src/kafka';

@injectable()
export abstract class BaseEventHandler {
  protected readonly consumer: Consumer;

  public constructor(@unmanaged() consumerFactory: IConsumerFactory, config: ConsumerConfig) {
    this.consumer = consumerFactory.createConsumer(config);
  }
}
