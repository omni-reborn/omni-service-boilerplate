import { inject, injectable } from 'inversify';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { IKafkaConnectorConfig } from 'src/config/kafka';
import { Kafka } from 'kafkajs';
import { exists } from 'src/util';

export interface IKafkaConnector {
  getClient(): Kafka;
}

@injectable()
export class KafkaConnector implements IKafkaConnector {
  private readonly config: IKafkaConnectorConfig;
  private client: Kafka;

  public constructor(@inject(CONFIG_IDENTIFIER.IKafkaConnectorConfig) config: IKafkaConnectorConfig) {
    this.config = config;
  }

  public getClient() {
    if (!exists(this.client)) {
      this.client = new Kafka({
        clientId: this.config['client-id'],
        brokers: this.config.brokers,
      });
    }

    return this.client;
  }
}
