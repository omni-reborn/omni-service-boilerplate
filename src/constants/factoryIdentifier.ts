export const FACTORY_IDENTIFIER = {
  IConsumerFactory: Symbol.for('IConsumerFactory'),
  IProducerFactory: Symbol.for('IProducerFactory'),
  ILoggerFactory: Symbol.for('ILoggerFactory'),
};
