export const CONFIG_IDENTIFIER = {
  IDatabaseConnectorConfig: Symbol.for('IDatabaseConnectorConfig'),
  IKafkaConnectorConfig: Symbol.for('IKafkaConnectorConfig'),
  IAppServiceConfig: Symbol.for('IAppServiceConfig'),
  IAuthServiceConfig: Symbol.for('IAuthServiceConfig'),
  IUserAuthProviderConfig: Symbol.for('IUserAuthProviderConfig'),
  ILoggerConfig: Symbol.for('ILoggerConfig'),
};
