import { ILogger } from './logger';
import { injectable, inject } from 'inversify';
import { SERVICE_IDENTIFIER } from 'src/constants';

export interface ILoggerFactory {
  createLogger(module: string): ILogger;
}

@injectable()
export class LoggerFactory implements ILoggerFactory {
  private readonly logger: ILogger;

  public constructor(@inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger) {
    this.logger = logger;
  }

  public createLogger(module: string) {
    return this.logger.createChildLogger(module);
  }
}

