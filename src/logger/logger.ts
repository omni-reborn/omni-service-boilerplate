import { injectable, inject, unmanaged } from 'inversify';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { ILoggerConfig } from 'src/config/logger';
import { exists } from 'src/util';

export enum LogLevel {
  Info = 'info',
  Warn = 'warning',
  Error = 'error',
}

export interface LogOptions {
  isTimestamp: boolean;
  isLevel: boolean;
  isModule: boolean;
}

export interface ILogger {
  info(message: string, data?: any, options?: LogOptions): void;
  warn(message: string, data?: any, options?: LogOptions): void;
  error(message: string, data?: any, options?: LogOptions): void;
  createChildLogger(module: string): ILogger;
  getModule(): string;
}

@injectable()
export class Logger implements ILogger {
  private readonly config: ILoggerConfig;
  private readonly module: string;
  private readonly parent: ILogger;
  private readonly options: LogOptions;

  public constructor(
    @inject(CONFIG_IDENTIFIER.ILoggerConfig) config: ILoggerConfig,
    @unmanaged() module: string,
    @unmanaged() parent?: ILogger,
  ) {
    this.config = config;
    this.module = module;
    this.parent = parent;
    this.options = {
      isTimestamp: config.defaults.timestamp,
      isLevel: config.defaults.level,
      isModule: config.defaults.module,
    };
  }

  private formatMessage(message: string, data: any, level: LogLevel, options: LogOptions) {
    let formattedMessage = { message, ...data };

    formattedMessage = options.isTimestamp ? { ...formattedMessage, ...{ timestamp: new Date() } } : formattedMessage;
    formattedMessage = options.isLevel ? { ...formattedMessage, ...{ level } } : formattedMessage;
    formattedMessage = options.isModule ? { ...formattedMessage, ...{ module: this.getModule() } } : formattedMessage;
    return JSON.stringify(formattedMessage);
  }

  private logMessage(message: string, data: any, level: LogLevel, options: LogOptions) {
    const messageOptions: LogOptions = { ...this.options, ...options };
    const outMessage = this.formatMessage(message, data, level, messageOptions);
    process.stdout.write(outMessage);
  }

  public info(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Info, options);
  }

  public warn(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Warn, options);
  }

  public error(message: string, data?: any, options?: LogOptions) {
    this.logMessage(message, data, LogLevel.Error, options);
  }

  public createChildLogger(module: string) {
    return new Logger(this.config, module, this);
  }

  public getModule() {
    if (exists(this.parent)) {
      return `${this.parent.getModule()}.${this.module}`;
    }

    return this.module;
  }
}
