import 'reflect-metadata';
import { InversifyExpressServer } from 'inversify-express-utils';
import { installContainer } from 'src/container/installer';
import { CONFIG_IDENTIFIER, SERVICE_IDENTIFIER } from './constants';
import { AppService } from './app';
import { IAppServiceConfig } from './config/app';
import { ILogger } from './logger';

async function main() {
  try {
    const container = await installContainer();

    const server = new InversifyExpressServer(container);
    const logger = container.get<ILogger>(SERVICE_IDENTIFIER.ILogger);
    const appConfig = container.get<IAppServiceConfig>(CONFIG_IDENTIFIER.IAppServiceConfig);

    const app = new AppService(server, logger, appConfig);
    await app.start();
  } catch (error) {
    console.log(error);
  }
}

/* tslint:disable: no-floating-promises */
main();
