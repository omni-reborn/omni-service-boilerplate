import { ILogger } from 'src/logger';
import { Request, Response, NextFunction } from 'express';
import { exists } from 'src/util';

export class LoggerMiddleWare {
  private readonly logger: ILogger;

  public constructor(logger: ILogger) {
    this.logger = logger;
  }

  public handle(req: Request, res: Response, next: NextFunction) {
    const ip = exists(req.ip) ? req.ip
      : exists(req.connection) ? req.connection.remoteAddress
      : 'unknown';
    this.logger.info('Incoming request', {
      ip,
      protocol: req.protocol,
      host: req.hostname,
      url: req.originalUrl,
      correlationId: req.body?.correlationId,
    });

    next();
  }
}
