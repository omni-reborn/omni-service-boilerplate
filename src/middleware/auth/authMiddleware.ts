import { BaseMiddleware } from 'inversify-express-utils';
import { ILogger, ILoggerFactory } from 'src/logger';
import { inject } from 'inversify';
import { FACTORY_IDENTIFIER } from 'src/constants';
import { Request, Response, NextFunction } from 'express';

export class AuthMiddleware extends BaseMiddleware {
  private readonly logger: ILogger;

  public constructor(@inject(FACTORY_IDENTIFIER.ILoggerFactory) loggerFactory: ILoggerFactory) {
    super();
    this.logger = loggerFactory.createLogger('authMiddleware');
  }

  public async handler(req: Request, res: Response, next: NextFunction) {
    const correlationId = req.body?.correlationId;
    const resource = req.originalUrl.replace('/', ':') + req.method;
    if (await this.httpContext.user.isResourceOwner(resource)) {
      this.logger.info('User is authorized to access resource.', { resource, correlationId });
      next();
    } else {
      const message = 'User not authorized to access resource.';
      this.logger.info(message, { resource, correlationId });
      res.status(401).json({ message });
    }
  }
}
