import { injectable, inject } from 'inversify';
import { interfaces } from 'inversify-express-utils';
import { Request, Response, NextFunction } from 'express';
import { SERVICE_IDENTIFIER, CONFIG_IDENTIFIER } from 'src/constants';
import { IAuthService } from 'src/service';
import { Principal } from './principal';
import { IUserAuthProviderConfig } from 'src/config/middleware/userAuthProviderConfig';


@injectable()
export class UserAuthProvider implements interfaces.AuthProvider {
  private readonly config: IUserAuthProviderConfig;
  private readonly authService: IAuthService;

  public constructor(
    @inject(CONFIG_IDENTIFIER.IUserAuthProviderConfig) config: IUserAuthProviderConfig,
    @inject(SERVICE_IDENTIFIER.IAuthService) authService: IAuthService,
  ) {
    this.config = config;
    this.authService = authService;
  }

  public async getUser(req: Request, res: Response, next: NextFunction) {
    const token = req.cookies[this.config['user-cookie-name']];
    const correlationId = req.body.correlationId;
    try {
      const userDetails = await this.authService.getUserDetails(token, correlationId);
      return new Principal(userDetails);
    } catch (error) {
      return null;
    }
  }
}
