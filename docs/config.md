# Config

![Omni Reborn Service Boilerplate](images/service-banner-wide.png)

Service configurations are written in `YAML` and allow to both customize the service behavior as well as to smooth out the process of injection of configs into its' various subservices.

All config files are located in the `/config` directory, although they are not linked automatically to the application - this process happens inside the [config module](../src/container/configModule.ts), where bindings for the IoC container are created.

Configs are also validated at load (using [io-ts](https://github.com/gcanti/io-ts)) - invalid configurations will result in an exception being raised, and ultimately - application shutdown.

![Omni Reborn Service Boilerplate](images/service-implementing-new-configs.png)

---

If you wish to implement another config, there is a few things you need to do:

1. Write a config definition file using [io-ts](https://github.com/gcanti/io-ts):
```ts
/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TExampleConfig>;
export interface IExampleConfig extends T { }

export const TExampleConfig = t.type({
  name: t.string,
  port: t.string,
});
```

*Note: The reason why we use an empty interface instead of a type is because of how typescript handles type inferrence - doing it this way gives us better intellisense. [Read more here!](https://github.com/gcanti/io-ts#typescript-integration)*

2. Add a config identifier to constants:
```ts
export const CONFIG_IDENTIFIER = {
  IExampleConfig: Symbol.for('IExampleConfig'),
};
```

3. Register the config file in the container:
```ts
export const configModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IExampleConfig>(CONFIG_IDENTIFIER.IExampleConfig)
      .toConstantValue(loadConfig<IExampleConfig>(TExampleConfig, './config/exampleConfig.yml'));
  },
);
```

4. Write the actual configuration file:
```yml
name: 'My Application'
port: '6900'
```
