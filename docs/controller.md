# Controller

![Omni Reborn Service Boilerplate](images/service-banner-wide.png)

Controllers, in the scope of Omni, can refer to two things: HTTP Controllers and Controllers that interact with the domain layer.

While the former might not exist in your specific service (no HTTP endpoints to handle), the latter almost always will. Do not worry, though - we cover both below.

![Omni Reborn Service Boilerplate](images/service-http-controllers.png)

---

HTTP Controllers are handled by [inversify-express-utils](https://github.com/inversify/inversify-express-utils). It provides us with a multitude of decorators to make our work of handling HTTP requests as easy as possible.

Implementing an HTTP Controller is very easy:
```ts
import { controller as httpController } from 'inversify-express-utils';

@httpController('/example')
export class ExampleHttpController {
  private readonly controller: IExampleController;

  public constructor(@inject(CONTROLLER_IDENTIFIER.IExampleController) controller: IExampleController) {
    this.controller = controller;
  }

  @httpPost('/')
  public async saveExample(@request() req: Request, @response() res: Response) {
    try {
      await this.controller.save(req.body);
      res.status(200).json({ message: 'Successfully saved example!' });
    } catch (error) {
      res.status(500).json({ error: 'Something went wrong' });
    }
  }
}
```

### **Important!**

Binding of HTTP Controllers in the container is handled entirely by `inversify-express-utils`. You *should not* add them to the container manually, all you need to do is to decorate them with `controller()` decorator.
They need to extend `BaseHttpController` to gain access to `HttpContext`, or have to have one injected.

Additionally, controllers need to be imported once - and once only. Importing them multiple times will result in an error.

You can read more about HTTP controllers in the [inversify-express-utils docs](https://github.com/inversify/inversify-express-utils#important-information-about-the-controller-decorator).

![Domain Controllers](images/service-controllers.png)

---

Domain controllers will be the interaction layer between the domain and the infra.
Unlike HTTP Controllers, these need to be registered manually in the DI and injected appropriately where needed (into HTTP Controllers or Kafka event handlers, for example).

Implementing Domain controllers is fairly simple, too:

```ts
export interface IExampleController {
  save(dto: ExampleDTO): Promise<void>;
}

export class ExampleController implements IExampleController extends BaseHttpController {
  private readonly repository: IExampleRepository;

  public constructor(@inject(REPOSITORY_IDENTIFIER.IExampleRepository) repository: IExampleRepository) {
    this.repository = repository;
  }

  @Transaction()
  public async save(dto: SaveRequestDTO) {
    const domain = new Example(dto);
    const exampleDTO = ExampleDataMapper.fromDomain(domain);
    
    await this.repository.save(exampleDTO);
  }
}
```

![Transactions](images/service-transactions.png)

---

Transactions are handled using the `Transaction()` decorator. Every repository call inside a method marked with this decorator will have a `Transactional Entity Manager` injected into it. 
These injections are not handled by the IoC container, but through a Continuation Local Storage, using [cls-hooked](https://github.com/jeff-lewis/cls-hooked).

For that reason, it is required to create a CLS namespace at application start (currently handled already inside the [App Service](../src/service/index.ts)).
Also, every single transaction method needs to be ran in the context of the namespace, like so:

```ts
const context = getNamespace(APP_IDENTIFIER.clsContext);
context.run(() => {
  // run controllers here
});
```

**For HTTP Controllers, this is already being handled per-request through express middleware.**

If that namespace is not available at the moment of transaction, or the decorated method has not been ran in scope of the namespace, an error will be thrown.