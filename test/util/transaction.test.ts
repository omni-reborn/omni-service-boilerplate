import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';

const expect = chai.expect;
import * as typeorm from 'typeorm';
import { Transaction } from 'src/util';
import { createNamespace, getNamespace, destroyNamespace } from 'cls-hooked';
import { mockConnection } from 'test/typeorm';
import { TransactionError } from 'src/error/database';
import { APP_IDENTIFIER } from 'src/constants';
import { expectToThrowAsync } from 'test/expectToThrow';


class MockService {
  @Transaction()
  public getManager() {
    const context = getNamespace(APP_IDENTIFIER.clsContext);
    return context.get(APP_IDENTIFIER.transactionalEntityManager);
  }
}

describe('Transaction decorator', () => {
  const sandbox = sinon.createSandbox();
  const context = createNamespace(APP_IDENTIFIER.clsContext);

  beforeEach(() => {
    const connection = mockConnection as any as typeorm.Connection;
    sandbox.stub(typeorm, 'getConnection').returns(connection);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Should set an entity manager in scope of a transaction', () => {
    const mockService = new MockService();
    context.run(async () => {
      const manager = await mockService.getManager();
      expect(manager).to.exist;
    });
  });

  it('Should throw a TransactionError when cls namespace has not been instantiated', () => {
    const mockService = new MockService();
    try {
      context.run(async () => {
        await mockService.getManager();
      });
    } catch (error) {
      expect(error).to.be.instanceOf(TransactionError);
    }
  });

  it('Should throw a TransactionError when transaction is ran outisde of the context scope', async () => {
    const mockService = new MockService();
    try {
      await mockService.getManager();
    } catch (error) {
      expect(error).to.be.instanceOf(TransactionError);
    }
  });

  it('Should throw a TransactionError when context does not exist when decorating', async () => {
    destroyNamespace(APP_IDENTIFIER.clsContext);
    const mockService = new MockService();
    try {
      await mockService.getManager();
    } catch (error) {
      expect(error).to.be.instanceOf(TransactionError);
    }
  });
});
