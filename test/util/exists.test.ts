import * as chai from 'chai';
import 'mocha';

const expect = chai.expect;
import { exists } from 'src/util';

describe('exists() utility', () => {
  it('Should return false for nullish values', () => {
    expect(exists(null)).to.be.false;
    expect(exists(undefined)).to.be.false;
  });

  it('Should return true for non-nullish values', () => {
    expect(exists('')).to.be.true;
    expect(exists(0)).to.be.true;
    expect(exists([])).to.be.true;
    expect(exists({})).to.be.true;
    expect(exists(/x/)).to.be.true;
    expect(exists(true)).to.be.true;
    expect(exists(Symbol)).to.be.true;
    expect(exists(new Date())).to.be.true;
    expect(exists(new Error())).to.be.true;
  });
});
