import * as sinon from 'sinon';
import { ProducerRecord, RecordMetadata } from 'kafkajs';

export interface IMockProducer {
  connect: sinon.SinonStub<null, Promise<void>>;
  disconnect: sinon.SinonStub<null, Promise<void>>;
  send: sinon.SinonStub<[ProducerRecord?], Promise<Array<RecordMetadata>>>;
}

export function createMockProducer(sandbox: sinon.SinonSandbox): IMockProducer {
  return {
    connect: sandbox.stub(),
    disconnect: sandbox.stub(),
    send: sinon.stub(),
  }
}
