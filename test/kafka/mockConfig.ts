import { IKafkaConnectorConfig } from 'src/config/kafka';

export const mockConfig: IKafkaConnectorConfig = {
  'client-id': 'test',
  brokers: ['localhost:9001', 'localhost:9002'],
};
