import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { ConsumerFactory, IConsumerFactory, KafkaConnector } from 'src/kafka';
import { Kafka, Consumer } from 'kafkajs';

const expect = chai.expect;

describe('Kafka Consumer Factory', () => {
  const sandbox = sinon.createSandbox();

  let factory: IConsumerFactory;
  let client: sinon.SinonStubbedInstance<Kafka>;

  beforeEach(() => {
    client = sandbox.createStubInstance(Kafka, {
      consumer: sandbox.stub().resolves(Kafka.prototype.consumer) as any as Consumer,
    });

    const connector = sandbox.createStubInstance(KafkaConnector, {
      getClient: sandbox.stub<[], Kafka>().returns(client),
    });

    factory = new ConsumerFactory(connector);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('createConsumer() should return a consumer', () => {
    const consumer = factory.createConsumer();
    expect(consumer).to.exist;
  });

  it('createConsumer() should create a unique consumer every time', () => {
    const consumer1 = factory.createConsumer();
    const consumer2 = factory.createConsumer();

    expect(consumer1).to.not.equal(consumer2);
  });
});
