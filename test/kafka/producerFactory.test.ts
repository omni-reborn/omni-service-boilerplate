import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';
import { KafkaConnector, IProducerFactory, ProducerFactory } from 'src/kafka';
import { Kafka, Producer } from 'kafkajs';

const expect = chai.expect;

describe('Kafka Producer Factory', () => {
  const sandbox = sinon.createSandbox();

  let factory: IProducerFactory;
  let client: sinon.SinonStubbedInstance<Kafka>;

  beforeEach(() => {
    client = sandbox.createStubInstance(Kafka, {
      producer: sandbox.stub().resolves(Kafka.prototype.producer) as any as Producer,
    });

    const connector = sandbox.createStubInstance(KafkaConnector, {
      getClient: sandbox.stub<[], Kafka>().returns(client),
    });

    factory = new ProducerFactory(connector);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('createProducer() should return a producer', () => {
    const producer = factory.createProducer();
    expect(producer).to.exist;
  });

  it('createProducer() should create a unique producer every time', () => {
    const producer1 = factory.createProducer();
    const producer2 = factory.createProducer();

    expect(producer1).to.not.equal(producer2);
  });
});
