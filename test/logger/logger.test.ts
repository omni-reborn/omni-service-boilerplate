import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';

const expect = chai.expect;

import { mockConfig } from './mockConfig';
import { Logger, LogLevel } from 'src/logger';

describe('Logger', () => {
  const sandbox = sinon.createSandbox();

  let stdoutMock: sinon.SinonStub;

  beforeEach(() => {
    stdoutMock = sandbox.stub(process.stdout, 'write');
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('info() should send a message to stdout and kafka using default options (timestamp & level)', () => {
    const logger = new Logger(mockConfig, 'test');
    const msg = 'test';
    logger.info(msg);

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module');
    expect(stdoutCall['message']).to.be.equal(msg);
    expect(stdoutCall['level']).to.be.equal(LogLevel.Info);

    stdoutMock.restore();
  });

  it('info() should accept additional data to be logged (correlationId & serviceName)', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.info('test', { correlationId: '1', serviceName: 'test' });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module', 'correlationId', 'serviceName');

    stdoutMock.restore();
  });

  it('info() should accept format options', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.info('test', null, { isLevel: false, isTimestamp: false, isModule: false });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.not.have.keys('timestamp', 'level', 'module');

    stdoutMock.restore();
  });

  it('warn() should send a message to stdout and kafka using default options (timestamp & level)', () => {
    const logger = new Logger(mockConfig, 'test');
    const msg = 'test';
    logger.warn(msg);

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module');
    expect(stdoutCall['message']).to.be.equal(msg);
    expect(stdoutCall['level']).to.be.equal(LogLevel.Warn);

    stdoutMock.restore();
  });

  it('warn() should accept additional data to be logged (correlationId & serviceName)', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.warn('test', { correlationId: '1', serviceName: 'test' });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module', 'correlationId', 'serviceName');

    stdoutMock.restore();
  });

  it('warn() should accept format options', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.warn('test', null, { isLevel: false, isTimestamp: false, isModule: false });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.not.have.keys('timestamp', 'level', 'module');

    stdoutMock.restore();
  });

  it('error() should send a message to stdout and kafka using default options (timestamp & level)', () => {
    const logger = new Logger(mockConfig, 'test');
    const msg = 'test';
    logger.error(msg);

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module');
    expect(stdoutCall['message']).to.be.equal(msg);
    expect(stdoutCall['level']).to.be.equal(LogLevel.Error);

    stdoutMock.restore();
  });

  it('error() should accept additional data to be logged (correlationId & serviceName)', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.error('test', { correlationId: '1', serviceName: 'test' });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.have.keys('message', 'timestamp', 'level', 'module', 'correlationId', 'serviceName');

    stdoutMock.restore();
  });

  it('error() should accept format options', () => {
    const logger = new Logger(mockConfig, 'test');
    logger.error('test', null, { isLevel: false, isTimestamp: false, isModule: false });

    const stdoutCall = JSON.parse(stdoutMock.getCall(0).args[0] as string);
    expect(stdoutCall).to.not.have.keys('timestamp', 'level', 'module');

    stdoutMock.restore();
  });

  it('getModule() should return a single-level module if logger has no parent', () => {
    const logger = new Logger(mockConfig, 'test');
    const module = logger.getModule();
    expect(module).to.be.equal('test');

    stdoutMock.restore();
  });

  it('getModule() should return a multi-level module if logger is a child logger', () => {
    const parentLogger = new Logger(mockConfig, 'parent');
    const childLogger = parentLogger.createChildLogger('child');
    const childOfChildLogger = childLogger.createChildLogger('childOfChild');

    const module = childOfChildLogger.getModule();
    expect(module).to.be.equal('parent.child.childOfChild');

    stdoutMock.restore();
  });
});
