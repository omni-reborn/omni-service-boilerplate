import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';

const expect = chai.expect;

import { DatabaseConnector, IDatabaseConnector } from 'src/database';
import { mockConfig } from './mockConfig';
import * as typeorm from 'typeorm';
import { DatabaseConnectionError, DatabaseRepeatConnectionError } from 'src/error/database';
import { expectToThrowAsync } from 'test/expectToThrow';

describe('Database Connector', () => {
  const sandbox = sinon.createSandbox();

  let connector: IDatabaseConnector;
  let createConnectionMock: sinon.SinonStub<[typeorm.ConnectionOptions], Promise<typeorm.Connection>>;

  beforeEach(() => {
    connector = new DatabaseConnector(mockConfig);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('Should create a database connection', async () => {
    createConnectionMock = sandbox.stub(typeorm, 'createConnection').resolves();
    await connector.connect();
    expect(createConnectionMock.calledOnce).to.be.true;
  });

  it('Should throw a connection error when attempting to establish a connection more than once', async () => {
    createConnectionMock = sandbox.stub(typeorm, 'createConnection').resolves();
    await connector.connect();
    expectToThrowAsync(() => connector.connect(), DatabaseRepeatConnectionError);
    expect(createConnectionMock.calledOnce).to.be.true;
  });

  it('Should throw a Connection Error when connection fails', async () => {
    createConnectionMock = sandbox.stub(typeorm, 'createConnection').rejects();
    expectToThrowAsync(() => connector.connect(), DatabaseConnectionError);
  });
});
